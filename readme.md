# Ephemerides

This is a library loosely based on the [Swift ZeitSatTrack package](https://github.com/dhmspector/ZeitSatTrack).  It is _very much_ a work in progress.  It's intent is to support a fun project I'm working on, and as ever, no warranty of any kind is expressed or implied.