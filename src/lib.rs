use latlon::*;
use color_eyre::eyre::Result;
use color_eyre::eyre::eyre;

mod satellite;

pub fn parse_latlon(string: &str) -> Result<Point>  {
    latlon::parse(string).map_err(|e| {
        eyre!("{}", e)
    })
}

#[derive(Debug, Clone)]
pub struct LookAngle {
    azimuth: f64,
    elevation: f64
}

#[cfg(test)]
mod tests {
    use color_eyre::Report;

    use super::*;

    #[test]
    fn it_works() {
        let result = parse_latlon("N45, W123");

        let rhs: Point = (-123., 45.).into();
        match result {
            Ok(lhs) => assert_eq!(lhs, rhs),
            Err(e) => assert!(false, "Failed to parse lat/lon: {}", e)
        };
    }

    #[test]
    fn it_breaks() {
        let result = parse_latlon("gibberish");
        match result {
            Ok(_) => assert!(false, "Unexpectedly succeeded provided gibberish input"),
            Err(_) => return()
        };
    }
}
